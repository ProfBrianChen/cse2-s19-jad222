//CSE2 HW10
//Julia Damron
// 4/30/19

import java.util.Random;

public class RobotCity{
	public static void main(String[] args){
		System.out.println();
		Random randy = new Random();
		int[][] city = buildCity();
		int a = city.length;
		int b = city[city.length - 1].length;
		display(city);
		int k = randy.nextInt(a*b);
		invade(city, k);
		display(city);
		for(int i = 0; i < 5; i++){
			update(city);
			display(city);
		} //end of for
	} //end of main method
	public static int[][] buildCity(){
		Random randy = new Random();
		int a = randy.nextInt(6) + 10;
		int b = randy.nextInt(6) + 10;
		int[][] city = new int[a][b];
		int rando = 0;
		for(int i = 0; i < a; i++){
			for(int j = 0; j < b; j++){
				rando = randy.nextInt(900) + 100;
				city[i][j] = rando;
			}//end of for
		} //end of for
		System.out.println("A city has been built!");
		return city;
	} //end of buildCity
	public static void display(int[][] city){
		String temp;
		for(int i = 0; i < city.length; i++){
			for(int j = 0; j < city[city.length - 1].length; j++){
				System.out.printf("%4d", city[i][j]);
				System.out.print(" ");
			} //end of for
			System.out.println();
		} //end of for
		System.out.println();
	} //end of display
	public static int[][] invade(int[][] city, int k){
		Random randy = new Random();
		int a = 0;
		int b = 0;
		for(int i = 0; i < k; i++){ //iterates through all robots
			a = randy.nextInt(city.length); //random ns coordinate
			b = randy.nextInt(city[city.length - 1].length); //random we coordinate
			while(city[a][b] < 0){ //runs if random coordinate already has robot present
				a = randy.nextInt(city.length); //random ns coordinate
				b = randy.nextInt(city[city.length - 1].length); //random we coordinate
			} //end of while
			city[a][b] = -1 * city[a][b]; //drops robot onto block coordinate
		} //end of for
		System.out.println("Oh no! The city has been invaded by robots!");
		return city;
	} //end of invade
	public static int[][] update(int[][] city){
		for(int i = 0; i < city.length; i++){ //ns
			for(int j = city[city.length - 1].length - 1; j > -1; j--){ //we
				if(city[i][j] < 0){ //if contains robot
					city[i][j] = -1 * city[i][j]; //removes robot
					if(j != city[city.length - 1].length - 1){ //if not on most eastern block
						city[i][j+1] = -1 * city[i][j+1]; //adds robot to next block to east
					} //end of if
				} //end of if
			} //end of for
		} //end of for
		System.out.println("An update on the city's robot invasion:");
		return city;
	} //end of update
} //end of class