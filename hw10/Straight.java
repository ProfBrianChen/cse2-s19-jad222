//CSE2 HW10
//Julia Damron
//4/30/19

import java.util.Random;
import java.util.Arrays;

public class Straight{
	public static void main(String[] args){
		int[] deck;
		int[] hand;
		int count = 0;
		for(int i = 0; i < 1000000; i++){ //randomly draws 1 million hands and checks if they are straights
			deck = shuffle();
			hand = draw(deck);
			count += straight(hand);
		} //end of for
		double percent = (count / 1000000) * 100; //calculate percantage of straights
		System.out.printf("The percentage of straights was %4.3f", percent); //prints the percentage of straights
		System.out.println("%%");
	} //end of main method
	public static int[] shuffle(){ //generates shuffled deck of cards
		Random randy = new Random();
		int rando;
		int[] deck = new int[52];
		boolean used = false;
		for(int i = 0; i < deck.length; i++){
			deck[i] = i;
		} //end of for
		for(int i = 0; i < deck.length; i++){ //shuffles the deck
			int rand = randy.nextInt(deck.length);
			int temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		} //end of for
		return deck;
	} //end of draw
	public static int[] draw(int[] deck){ //draws 5 random cards
		int[] hand = new int[5];
		for(int i = 0; i < hand.length; i++){
			hand[i] = deck[i];
		} //end of for
		for(int i = 0; i < hand.length; i++){
			if(hand[i] > 12 && hand[i] < 26){
				hand[i] -= 13;
			} //end of if
			else if(hand[i] > 25 && hand[i] < 39){
				hand[i] -= 26;
			} //end of else if
			else if(hand[i] > 38){
				hand[i] -= 39;
			} //end of else if
			Arrays.sort(hand);
		} //end of for
		return hand;
	}
	public static int checkLow(int[] hand, int k){
		if(k < 1 || k > 5){
			System.out.println("Error: k is out of bounds");
			return -1;
		} //end of if
		Arrays.sort(hand);
		int low = hand[k-1];
		return low;
	} //end of checkLow
	public static int straight(int[] hand){
		//if straight, return 1. else return 0
		int count = 0;
		int low;
		for(int i = 0; i < hand.length; i++){
			low = checkLow(hand, i+1);
			if(i != 0){
				if(low == hand[i-1] + 1){
					count += 1;
				} //end of if
			} //end of if
		} //end of for
		if(count == 4){
			return 1;
		} //end of if
		else{
			return 0;
		} //end of else
	} //end of straight
} //end of class