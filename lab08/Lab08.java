//CSE2 Lab8
//Julia Damron
// 4/5/19

import java.util.Arrays;
import java.util.Random;
import java.lang.Math;

public class Lab08{
	public static void main(String[] args){
		Random randy = new Random();
		int rando = randy.nextInt(51) + 50; //randomly generates number from 50-100 inclusive
		int [] array = new int [rando];
		System.out.println("Random array:");
		for( int i  =0; i < array.length; i++ ){ //fills array with random ints between 0-99 inclusive
			rando = randy.nextInt(100);
			array[i] = rando;
			System.out.print(array[i] + " "); //prints array
		} //end for
		System.out.println(); //new line
		int range = getRange( array );
		int mean = getMean( array );
		double stdDev = getStdDev( range, mean, array );
		shuffle( array );
	} //end of main method
	public static int getRange( int [] array ){
		Arrays.sort(array);
		int min = array [0];
		int max = array [array.length - 1];
		int range = max - min;
		System.out.println("Range: " + range); //prints range
		return range;
	} //end of getRange
	public static int getMean( int [] array ){
		int sum = 0;
		for(int i = 0; i < array.length; i++){ //add up contentsof array
			sum = sum + array[i];
		} //end of for
		int mean = sum / array.length;
		System.out.println("Mean: " + mean); //prints mean
		return mean;
	} //end of getMean
	public static double getStdDev( int range, int mean , int [] array){
		double sum = 0;
		double dist = 0;
		double squared = 0;
		for(int i = 0; i < array.length; i++){ //sums deviation from mean
			dist = array[i] - mean;
			squared = dist * dist;
			sum += squared;
		} //end for
		double dev = sum / (array.length - 1);
		double stdDev = Math.sqrt( dev );
		System.out.println("Standard Deviation: " + stdDev);
		return stdDev;
	} //end of getStdDev
	public static void shuffle( int [] array ){ //shuffles array
		int target = 0;
		int temp = 0;
		Random randy = new Random();
		for(int i = 0; i < array.length; i++){
			target = (int) ( randy.nextInt(array.length) );
			temp = array [target];
			array [target] = array [i];
			array [i] = temp;
		} //end of for
		System.out.println("Shuffled array: ");
		for(int i = 0; i < array.length; i++){ //prints shuffled array
			System.out.print( array[i] + " " );
		} //end of for
    System.out.println();
	} //end of shuffle
} //end of class