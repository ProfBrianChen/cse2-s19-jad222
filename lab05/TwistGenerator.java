//CSE2 Lab 6
//Julia Damron
// 3/1/19
import java.util.Scanner;
public class TwistGenerator{ //class
  public static void main(String[] args){ //main method
    Scanner scanny = new Scanner( System.in );
    int length;
    String trash;
    int count = 0;
    
    System.out.print("Enter a positive integer: "); //prompts user to enter int
    
    //check if user input is a positive int:
    while ((scanny.hasNextInt() == false) || (scanny.nextInt() < 0)){
      System.out.print("The value you entered was not a valid input. Please enter a positive integer: "); //prompts user to enter int
      trash = scanny.next();
    }
    
    length = scanny.nextInt(); //set user input to length of twist
    int num = length;
    int mod = num % 3;
    
    //print first line of twist:
    while (num > 3){
      System.out.print("\\ /");
      num -= 3;
    } //end of while loop
  
    while (count < num){
      switch (mod){
        case 0:
          System.out.print("\\ /");
          count += 3;
          break;
        case 1:
          System.out.print("\\");
          count += 1;
          break;
        case 2:
          System.out.print("\\ ");
          count += 2;
          break;
      } //end of switch
    } //end of while loop
    
    System.out.println(""); //goes to next line
    count = 0; //reset count
    num = length; //reset num
    
    //print second line of twist:
    while (num > 3){
      System.out.print(" X ");
      num -= 3;
    } //end of while loop
  
    while (count < num){
      switch (mod){
        case 0:
          System.out.print(" X ");
          count += 3;
          break;
        case 1:
          System.out.print(" ");
          count += 1;
          break;
        case 2:
          System.out.print(" X");
          count += 2;
          break;
      } //end of switch
    } //end of while loop
    
    System.out.println(""); //goes to next line
    count = 0; //reset count
    num = length; //reset num
    
    //print third line of twist:
    while (num > 3){
      System.out.print("/ \\");
      num -= 3;
    } //end of while loop
  
    while (count < num){
      switch (mod){
        case 0:
          System.out.print("/ \\");
          count += 3;
          break;
        case 1:
          System.out.print("/");
          count += 1;
          break;
        case 2:
          System.out.print("/ ");
          count += 2;
          break;
      } //end of switch
    } //end of while loop
    
    System.out.println("");
    System.out.println("The value you entered was: " + length);
  } //end of main method
} //end of class