// CSE2 HW 3
// Julia Damron
// 2/12/19

import java.util.Scanner; //imports Scanner class

public class BoxVolume{ //creates class
  public static void main(String[] args){ //creates main method
    Scanner scanny = new Scanner ( System.in ); //creates new Scanner
    System.out.print("\n" + "Enter the length of the box: "); //prompts user for length of box
    double length = scanny.nextDouble(); //stores user input as length
    System.out.print("Enter the width of the box: "); //prompts user for width of box
    double width = scanny.nextDouble(); //stores user input as width
    System.out.print("Enter the height of the box: "); //prompts user for height of box
    double height = scanny.nextDouble(); //stores user input as height;
    double volume = length * width * height; //calculates volume of box
    System.out.println("\n" + "The length of the box is: " + length); //prints length of box
    System.out.println("The width of the box is: " + width); //prints width of box
    System.out.println("The height of the box is: " + height + "\n"); //prints height of box
    System.out.println("The volume of the box is: " + volume + "\n"); //prints volume of box
  } //ends main method
} // ends class
