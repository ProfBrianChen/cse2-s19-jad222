// CSE2 HW3
// Julia Damron
// 2/12/19

import java.util.Scanner; //imports Scanner class

public class Convert { //creates class
  public static void main (String[] args){ //main method
    Scanner myScanner = new Scanner ( System.in ); //creates new Scanner
    System.out.print("Enter the distance in meters: "); //prompts user to enter distance in meters
    double meters = myScanner.nextDouble(); //stores user input as double called meters
    double inchesPerMeter = 39.3701; //constant inches per meter
    double inches = inchesPerMeter * meters; //converts distance in meters to inches
    inches = inches * 10000;
    inches = (int) inches;
    inches = (double) inches;
    inches = inches / 10000;
    System.out.println(meters + " meters is " + inches + " inches"); //prints distance in meters and inches
  } //end of main method
} //end of class