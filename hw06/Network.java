//CSE2 HW6
//Julia Damron
// 3/19/19

import java.util.Scanner;

public class Network{
	public static void main(String[] args){
		int height, width, square, edge;
		boolean input = false;
		Scanner scanny = new Scanner( System.in ); //initialize new scanner

		//prompt user for height and check for positive int
		do{
			System.out.print("Input your desired height: ");
			if(!scanny.hasNextInt()){
				System.out.println("Error: please type in an integer.");
				scanny.next();
			continue;
			} //end of if
			if ( height <= 0 ){
				System.out.print("Error: not a positive integer. ");
				input = false;
			}
		} //end of do
		while(!input);
		height = scanny.nextInt(); //set height to input

		
		//prompt user for width and check for positive int
		do{
			System.out.print("Input your desired width: ");
			if(!scanny.hasNextInt()){
				System.out.println("Error: please type in an integer.");
				scanny.next();
				continue;
			} //end of if
			if ( width <= 0 ){
				System.out.print("Error: not a positive integer. ");
				input = false;
			}
		} //end of do
		while(!input);
		width = scanny.nextInt(); //set width to input

		
		//prompt user for square size and check for positive int
		do{
			System.out.print("Input square size: ");
			if(!scanny.hasNextInt()){
				System.out.println("Error: please type in an integer.");
				scanny.next();
				continue;
			} //end of if
			if ( width <= 0 ){
				System.out.print("Error: not a positive integer. ");
				input = false;
			}
		} //end of do
		while(!input);
		square = scanny.nextInt(); //set square size to input
 
		//prompt user for edge length and check for positive int
		do{
			System.out.print("Input square size: ");
			if(!scanny.hasNextInt()){
				System.out.println("Error: please type in an integer.");
				scanny.next();
				continue;
		} //end of if
			if ( width <= 0 ){
				System.out.print("Error: not a positive integer. ");
				input = false;
			}
		} //end of do
		while(!input);
		edge = scanny.nextInt(); //set edge length to input

		
		//height = num rows, width = num columns, square size = height of square, edge length = num lines between squares
		//print pattern:
		int i = 0;//initialize variable for tracking horizontal place within pattern (not entire code)
		int j = 0;//initialize variable for tracking vertical place within pattern (not entire code)
		int a = square - 1;
		int b = square / 2;
		int c = (square / 2) + 1;
		for(int row = 0; row < height; row++){
			if ( j == 0 ){ //first row of pattern
				for(int space = 0; space < width; space++){
					if ( i == 0 ){ //first space in square
						System.out.print("#");
					}
					else if ( i == a ){ //last space in square
						System.out.print("#");
					}
					else{
						if( i < square){
							System.out.print("-"); //for spaces along square's edge
						}
						else{
							System.out.print(" "); //for spaces in between squares
						}
					}
					i++;
					if (i == (square + edge)){
						i = 0;
					}//end if
				}//end for(space)
			} //end if first row
			else if ( j == a ){ //last line of square in pattern
				for(int space = 0; space < width; space++){
					if ( i == 0 ){ //first space in square
						System.out.print("#");
					}
					else if ( i == a ){ //last space in square
						System.out.print("#");
					}
					else{
						if( i < square){
							System.out.print("-"); //for spaces along square's edge
						}
						else{
							System.out.print(" "); //for spaces in between squares
						}
					}
					i++;
					if (i == (square + edge)){
						i = 0;
					}//end if
				}//end for(space)
			}
			else if ( j == b ){ //middle line of square
				if(square%2 == 0){
					for(int space = 0; space < width; space++){
						if ( i == 0 ){
							System.out.print("|");
						}
						else if ( i == a ){
							System.out.print("|");
						}
						else{
							if (i >= square && i < (square + edge)){
								System.out.print("-");
							} //end if
							else{
								System.out.print(" ");
							}//end else
						} //end else
					} //end for (space)
				} //end if even
				else{
					for(int space = 0; space < width; space++){
						if ( i == 0 ){
							System.out.print("|");
						}
						else if ( i == a ){
							System.out.print("|");
						}
						else{
							System.out.print(" ");
						}
					} //end for (space)
				} //end else
				i++;
				if (i == (square + edge)){
					i = 0;
				} //end if
			}
			else if ( j == c ){ //middle line of square if even num
				for(int space = 0; space < width; space++){
					if ( i == 0 ){
						System.out.print("|");
					}
					else if ( i == a ){
						System.out.print("|");
					}
					else{
						if (i >= square && i < (square + edge)){
							System.out.print("-");
						} //end if
						else{
							System.out.print(" ");
						} //end else
					} //end else
					i++;
					if (i == (square + edge)){
						i = 0;
					} //end if
				} //end for (space)
			}
			else{ //in between lines
				if (i < square){ //generic row in square
					for(int space = 0; space < width; space++){
						if ( i == 0 ){
							System.out.print("|");
						}
						else if ( i == a ){
							System.out.print("|");
						}
						else{
							System.out.print(" ");
						}
					} //end for (space)
				} //end if (< square)
				else{ //generic row containing edge (in between squares)
					for(int space = 0; space < width; space++){
						if ( i == b ){
							if(square%2 == 0){
								System.out.print("|");
							} //end if
							else{
								System.out.print(" ");
							}
						}
						else if ( i == c ){
							System.out.print("|");
						}
						else{
							System.out.print(" ");
						}
					} //end for (space)
				} //end else (i)
				i++;
				if (i == (square + edge)){
					i = 0;
				} //end if
			} //end else (j)
			j++;
			if (j >= (square + edge)){
				j = 0;
			} //end if
			System.out.println();//begins new line
		}//end for(row)
	} //end of main method
} // end of class