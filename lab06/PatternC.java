//CSE2 Lab 6
//Julia Damron
// 3/8/19

import java.util.Scanner;

public class PatternC{
  public static void main(String[] args){
    Scanner scanny = new Scanner (System.in);
    int in =50;
    boolean input = false;
    do{
      System.out.print("Enter an integer between 1 and 10: ");
      if(!scanny.hasNextInt()){
        System.out.print("Error: Not an int. ");
        scanny.next();
        continue;
      }
      in = scanny.nextInt();
      if(in < 1 || in > 10){
        System.out.print("Error: Not within range. ");
        input = false;
      }
      else{
        input = true;
      }
    }
    while(!input);
    int num = 1;
    for(int j = in; j > 0; j--){
      for(int i = 10; i > 0; i--){
        if(i <= num){
          System.out.print(i);
        }
        else{
          System.out.print(" ");
        }
      }
      num++;
      System.out.println();
    }
  } //end of main method
} //end of class