//CSE2 Lab 6
//Julia Damron
// 3/8/19

import java.util.Scanner;

public class PatternA{
  public static void main(String[] args){
    Scanner scanny = new Scanner (System.in);
    String trash = "";
    int in =50;
    boolean input = false;
    do{
      System.out.print("Enter an integer between 1 and 10: ");
      if(!scanny.hasNextInt()){
        System.out.print("Error: Not an int. ");
        scanny.next();
        continue;
      }
      in = scanny.nextInt();
      if(in < 1 || in > 10){
        System.out.print("Error: Not within range. ");
        input = false;
      }
      else{
        input = true;
      }
    }
    while(!input);
    String temp = "";
    for(int i = 1; i<in+1; i++){
      temp = temp + i + " ";
      System.out.println(temp);
    }
  } //end of main method
} //end of class