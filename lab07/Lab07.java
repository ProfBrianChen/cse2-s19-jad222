//CSE2 Lab 7
//Julia Damron
// 3/22/19

import java.util.Random;
import java.util.Scanner;

public class Lab07{
	public static void main(String[] args){ //prints thesis sentence, asks user if they want another one until they say no, then prints paragraph
		String subj, trash;
		Scanner scanny = new Scanner ( System.in ); //new scanner
		boolean no = false; //used for do while
		subj = thesis(); //run thesis and determine subject
		do{
			System.out.println("Would you like another sentence? (Y/N):"); //ask user if they want another sentence
			if (!scanny.hasNext()){ //user did not input string
				System.out.println("Error: incorrect input type.");
				continue;
			} //end if
			trash = scanny.next(); //sets trash string to check input
			if (trash.equals("Y")){ //user wants another sentence
				subj = thesis();
				continue;
			} //end if
			else if (trash.equals("N")){ //user does not want another sentence
				no = true;
			} //end else if
			else{ //user entered an incorrect string
				System.out.println("Error: incorrect input.");
			} //end else
		} //end do
		while(!no); //condition for do-while
		Random randy = new Random(); //new random
		int num = randy.nextInt(6); //randomly generate int from 0-5
		for (int n = 0; n < num; n++){ //prints an action sentence num times
			act(subj);
		} //end for
		con(subj); //prints conclusion sentence
	} //end of main method
	public static String con(String subj){ //prints conclusion sentence
		String sub = subj;
		String ad = adj();
		String ob = obj();
		System.out.println("That " + sub + " was right about the " + ad + " " + ob + "!");
		return sub;
	} //end of con
	public static String act(String subj){ //prints action sentence
		String sub = subj;
		String adj1 = adj();
		String adj2 = adj();
		String ver = verb();
		String ob1 = obj();
		String ob2 = obj();
		Random randy = new Random();
		int num = randy.nextInt(3);
		if (num % 2 == 0){
			System.out.println("This " + sub + " was " + adj1 + " towards the " + adj2 + " " + ob1 + ".");
		} //end if
		else{
			System.out.println("It " + ver + " at the " + adj1 + " " + ob1 + " with a " + adj2 + " " + ob2 + ".");
		} //end else
		return subj;
	} //end of act
	public static String thesis(){ //prints thesis sentence
		String adj1, adj2, subj, ob, ver, next;
		Scanner scanny = new Scanner( System.in );
		boolean again = true;
			adj1 = adj();
			adj2 = adj();
			subj = sub();
			ob = obj();
			ver = verb();
			System.out.println("The " + adj1 + " " + subj + " " + ver + " the " + adj2 + " " + ob + ".");
			return subj;
	} //end of thesis
	public static String adj(){ //randomly selects ad returns an adjective
		Random randy = new Random();
		int num = randy.nextInt(10);
		String word = null;
		switch( num ){
			case 0:
				word = "dumb";
				break;
			case 1:
				word = "hard-of-hearing";
				break;
			case 2:
				word = "speedy";
				break;
			case 3:
				word = "broke";
				break;
			case 4:
				word = "sun-kissed";
				break;
			case 5:
				word = "warm";
				break;
			case 6:
				word = "funny";
				break;
			case 7:
				word = "thick";
				break;
			case 8:
				word = "short";
				break;
			case 9:
				word = "mad";
				break;
		} //end of switch
		return word;
	} //end of adj
	public static String sub(){ //rendomly selects and returns a subject noun
		Random randy = new Random();
		int num = randy.nextInt(10);
		String word = null;
		switch( num ){
			case 0:
				word = "alpaca";
				break;
			case 1:
				word = "llama";
				break;
			case 2:
				word = "coach";
				break;
			case 3:
				word = "Barney";
				break;
			case 4:
				word = "clock tower";
				break;
			case 5:
				word = "boy band";
				break;
			case 6:
				word = "band aid";
				break;
			case 7:
				word = "moose";
				break;
			case 8:
				word = "duck";
				break;
			case 9:
				word = "goose";
				break;
		} //end of switch
		return word;
	} //end of sub
	public static String verb(){ //randomly selects and returns a past tense verb
		Random randy = new Random();
		int num = randy.nextInt(10);
		String word = null;
		switch( num ){
			case 0:
				word = "raced";
				break;
			case 1:
				word = "sat on";
				break;
			case 2:
				word = "gossiped about";
				break;
			case 3:
				word = "traveled with";
				break;
			case 4:
				word = "colored with";
				break;
			case 5:
				word = "socked";
				break;
			case 6:
				word = "climbed";
				break;
			case 7:
				word = "ran to";
				break;
			case 8:
				word = "manufactured";
				break;
			case 9:
				word = "tanned";
				break;
		} //end of switch
		return word;
	} //end of verb
	public static String obj(){ //randomly selects and returns an object noun
		Random randy = new Random();
		int num = randy.nextInt(10);
		String word = null;
		switch( num ){
			case 0:
				word = "tree";
				break;
			case 1:
				word = "branch";
				break;
			case 2:
				word = "turtle";
				break;
			case 3:
				word = "rice";
				break;
			case 4:
				word = "spinach";
				break;
			case 5:
				word = "treadmill";
				break;
			case 6:
				word = "boat";
				break;
			case 7:
				word = "river";
				break;
			case 8:
				word = "pig";
				break;
			case 9:
				word = "smoothie";
				break;
		} //end of switch
		return word;
	} //end of obj
} //end of class