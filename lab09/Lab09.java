//CSE2 Lab9
//Julia Damron
// 4/12/19

import java.util.Random;
import java.util. Arrays;
import java.util.Scanner;

public class Lab09{
	public static void  main(String[] args){
		Scanner scanny = new Scanner( System.in ); //creates new Scanner
		boolean check = false;
		String answer = "";
		int choice = 0;
		int index = 0;
		int size = 0;
		int val = 0;
		do{
			System.out.print("Linear or binary search? "); //prompts user for linear or binary
			if( !scanny.hasNext() ){
				System.out.println( "Error, try again." );
				continue;
			} //end of if
			answer = scanny.next();
			if( answer.equals( "linear" ) || answer.equals( "Linear" ) ){
				choice = 1;
				check = true;
			} //end of if
			else if( answer.equals( "binary" ) || answer.equals( "Binary" ) ){
				choice = 2;
				check = true;
			} //end of else if
			else{
				System.out.println( "Error, try again." );
			} //end of else
		}
		while( !check ); //condition for do
		System.out.print( "Size: " );
		size = scanny.nextInt();
		System.out.print( "Search for: " );
		val = scanny.nextInt();
		int [] array = new int [ size ];
		if ( choice == 1 ){
			array = a( size );
			index = c( array, val );
		} //end of if
		else if ( choice == 2 ){
			array = b( size );
			index = d( array, val );
		} //end of else if
		System.out.println( "Array: " );
		for( int i = 0; i < array.length; i++){ //prints array
			System.out.print( array [i] + " " );
		} //end of for
		System.out.println(); //new line
		System.out.println( "Index: " + index );
	} //end of main method
	public static int [] a( int size ){
		Random randy = new Random();
		int rando = 0;
		int [] array = new int [ size ];
		for( int i = 0; i < size; i++ ){ //fill array with random ints
			rando = randy.nextInt( size + 1 );
			array [i] = rando;
		} //end of for
		return array;
	} //end of method one
	public static int [] b( int size ){
		Random randy = new Random();
		int rando = 0;
		int [] array = new int [ size ];
		for( int i = 0; i < size; i++ ){ //fill array with random ints
			rando = randy.nextInt( size + 1 );
			array [i] = rando;
		} //end of for
		Arrays.sort( array ); //sorts array elements into ascending order
		return array;
	} //end of method two
	public static int c( int [] array, int val ){ //linear search
		//int i = 0;
		int index = -1;
		boolean found = false;
		/*do{ //search array for val
			if( array [i] == val ){
				found = true;
			} //end of if
			else if( i == (array.length - 1) ){
				break;
			} //end of else if
			else{
				i ++;
			} //end of else
		} //end of do
		while( !found ); //condition for do */
		for( int i = 0; i < array.length; i++){ //search array for val
			if( array [i] == val ){
				index = i;
				found = true;
				break;
			} //end of if
		} //end of for
		return index;
	} //end of method three
	public static int d( int [] array, int val ){ //binary search
		Arrays.sort( array ); //sorts array
		int start = 0;
		int end = array.length - 1;
		int mid = ( start + end ) / 2;
		int index = -1;
		while( start <= end ){
			if ( array [mid] == val ){ //val is found
				index = mid;
				break;
			} //end of if
			else if ( array [mid] < val ){ //avl is in top half of array
				start = mid + 1;
			} //end of else if
			else{
				end = mid - 1;
			} //end of else
			mid = ( start + end ) / 2;
		} //end of while
		return index;
	} //end of method four
} //end of class