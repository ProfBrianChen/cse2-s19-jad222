// CSE2 Lab 2
// Julia Damron
// 2/1/19

//record time in s and num rotations
//print: number of minutes for each trip, number of counts for each trip, distance of each trip in miles, distance for the two trips combined

public class Cyclometer{
  
  //main method
  public static void main(String[] args){
    
    //input data
    int secsTrip1=480;  //time for trip 1
    int secsTrip2=3220;  //time for trip 2
		int countsTrip1=1561;  //counts for trip 1
		int countsTrip2=9037; //counts for trip 2
    double wheelDiameter=27.0,  //diameter of wheel
    
    //useful constants + conversions
  	PI=3.14159, //pi constant
  	feetPerMile=5280,  //num feet per mile
  	inchesPerFoot=12,   //num inches per foot
  	secondsPerMinute=60;  //num secs per min
	  double distanceTrip1, distanceTrip2, totalDistance;  //distance traveled in trip 1, 2, and both
    
    //print trip 1 time and counts
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
    //print trip 2 time and counts
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
    
    //calculations and conversions
    //distance trip 1 in inches below: 1 rotation travels (diameter*PI) inches
    distanceTrip1=countsTrip1*wheelDiameter*PI;
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    //distance trip 2 in miles below: 1 rotation travels (diameter*PI) miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2; //calculates total distance by adding distance 1 and 2
    
    //print distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints trip 1 distance in miles
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints trip 2 distance in miels
	  System.out.println("The total distance was "+totalDistance+" miles"); //prints total distance in miles

  } //end of main method
  
} //end of class
