//CSE2 HW 4
//Julia Damron
// 2/19/19

public class PokerHandCheck{
  public static void main(String[] args){

    int num1, num2, num3, num4, num5; //initializes ints num for use in switch statement
    
     //initialize strings for numbers on cardNums
    String cardNum1 = " ";
    String cardNum2 = " ";
    String cardNum3 = " ";
    String cardNum4 = " ";
    String cardNum5 = " ";
    
    String suit1, suit2, suit3, suit4, suit5; //introduces string used for suits
    
    boolean pair = false;
    boolean twoPair = false;
    boolean threeKind = false;
    
    String hand;

    num1 = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    num2 = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    num3 = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    num4 = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    num5 = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    
    //assign suits based on value of num1
    if (num1 >= 1 && num1 <= 13){
      suit1 = "Diamonds";
    } //end of if statement
    else if (num1 > 13 && num1 <= 26){
      suit1 = "Clubs";
      num1 = num1 - 13;
    } //end of if statement
    else if (num1 > 26 && num1 < 38){
      suit1 = "Hearts";
      num1 = num1 - 26;
    } //end of if statement
    else{
      suit1 = "Spades";
      num1 = num1 - 38;
    } //end of else statement
    
    //assign suits based on value of num2
    if (num2 >= 1 && num2 <= 13){
      suit2 = "Diamonds";
    } //end of if statement
    else if (num2 > 13 && num2 <= 26){
      suit2 = "Clubs";
      num2 = num2 - 13;
    } //end of if statement
    else if (num2 > 26 && num2 < 38){
      suit2 = "Hearts";
      num2 = num2 - 26;
    } //end of if statement
    else{
      suit2 = "Spades";
      num2 = num2 - 38;
    } //end of else statement
    
    //assign suits based on value of num3
    if (num3 >= 1 && num3 <= 13){
      suit3 = "Diamonds";
    } //end of if statement
    else if (num3 > 13 && num3 <= 26){
      suit3 = "Clubs";
      num3 = num3 - 13;
    } //end of if statement
    else if (num3 > 26 && num3 < 38){
      suit3 = "Hearts";
      num3 = num3 - 26;
    } //end of if statement
    else{
      suit3 = "Spades";
      num3 = num3 - 38;
    } //end of else statement
    
    //assign suits based on value of num4
    if (num4 >= 1 && num4 <= 13){
      suit4 = "Diamonds";
    } //end of if statement
    else if (num4 > 13 && num4 <= 26){
      suit4 = "Clubs";
      num4 = num4 - 13;
    } //end of if statement
    else if (num4 > 26 && num4 < 38){
      suit4 = "Hearts";
      num4 = num4 - 26;
    } //end of if statement
    else{
      suit4 = "Spades";
      num4 = num4 - 38;
    } //end of else statement
    
    //assign suits based on value of num5
    if (num5 >= 1 && num5 <= 13){
      suit5 = "Diamonds";
    } //end of if statement
    else if (num5 > 13 && num5 <= 26){
      suit5 = "Clubs";
      num5 = num5 - 13;
    } //end of if statement
    else if (num5 > 26 && num5 < 38){
      suit5 = "Hearts";
      num5 = num5 - 26;
    } //end of if statement
    else{
      suit5 = "Spades";
      num5 = num5 - 38;
    } //end of else statement

    //assign card numbers 1
    switch (num1){
      case 1 : cardNum1 = "Ace";
        break;
      case 2 : cardNum1 = "2";
        break;
      case 3 : cardNum1 = "3";
        break;
      case 4 : cardNum1 = "4";
        break;
      case 5 : cardNum1 = "5";
        break;
      case 6 : cardNum1 = "6";
        break;
      case 7 : cardNum1 = "7";
        break;
      case 8 : cardNum1 = "8";
        break;
      case 9 : cardNum1 = "9";
        break;
      case 10 : cardNum1 = "10";
        break;
      case 11 : cardNum1 = "Jack";
        break;
      case 12: cardNum1 = "Queen";
        break;
      case 13 : cardNum1 = "King";
        break;
      default : cardNum1 = "Ace";
        break;
    } //end of switch statement 1
    
    //assign card numbers 2
    switch (num2){
      case 1 : cardNum2 = "Ace";
        break;
      case 2 : cardNum2 = "2";
        break;
      case 3 : cardNum2 = "3";
        break;
      case 4 : cardNum2 = "4";
        break;
      case 5 : cardNum2 = "5";
        break;
      case 6 : cardNum2 = "6";
        break;
      case 7 : cardNum2 = "7";
        break;
      case 8 : cardNum2 = "8";
        break;
      case 9 : cardNum2 = "9";
        break;
      case 10 : cardNum2 = "10";
        break;
      case 11 : cardNum2 = "Jack";
        break;
      case 12: cardNum2 = "Queen";
        break;
      case 13 : cardNum2 = "King";
        break;
      default : cardNum2 = "2";
        break;
    } //end of switch statement 2
    
    //assign card numbers 3
    switch (num3){
      case 1 : cardNum3 = "Ace";
        break;
      case 2 : cardNum3 = "2";
        break;
      case 3 : cardNum3 = "3";
        break;
      case 4 : cardNum3 = "4";
        break;
      case 5 : cardNum3 = "5";
        break;
      case 6 : cardNum3 = "6";
        break;
      case 7 : cardNum3 = "7";
        break;
      case 8 : cardNum3 = "8";
        break;
      case 9 : cardNum3 = "9";
        break;
      case 10 : cardNum3 = "10";
        break;
      case 11 : cardNum3 = "Jack";
        break;
      case 12: cardNum3 = "Queen";
        break;
      case 13 : cardNum3 = "King";
        break;
      default : cardNum3 = "3";
        break;
    } //end of switch statement 3
    
    //assign card numbers 4
    switch (num4){
      case 1 : cardNum4 = "Ace";
        break;
      case 2 : cardNum4 = "2";
        break;
      case 3 : cardNum4 = "3";
        break;
      case 4 : cardNum4 = "4";
        break;
      case 5 : cardNum4 = "5";
        break;
      case 6 : cardNum4 = "6";
        break;
      case 7 : cardNum4 = "7";
        break;
      case 8 : cardNum4 = "8";
        break;
      case 9 : cardNum4 = "9";
        break;
      case 10 : cardNum4 = "10";
        break;
      case 11 : cardNum4 = "Jack";
        break;
      case 12: cardNum4 = "Queen";
        break;
      case 13 : cardNum4 = "King";
        break;
      default : cardNum4 = "4";
        break;
    } //end of switch statement 4
    
    //assign card numbers 5
    switch (num5){
      case 1 : cardNum5 = "Ace";
        break;
      case 2 : cardNum5 = "2";
        break;
      case 3 : cardNum5 = "3";
        break;
      case 4 : cardNum5 = "4";
        break;
      case 5 : cardNum5 = "5";
        break;
      case 6 : cardNum5 = "6";
        break;
      case 7 : cardNum5 = "7";
        break;
      case 8 : cardNum5 = "8";
        break;
      case 9 : cardNum5 = "9";
        break;
      case 10 : cardNum5 = "10";
        break;
      case 11 : cardNum5 = "Jack";
        break;
      case 12: cardNum5 = "Queen";
        break;
      case 13 : cardNum5 = "King";
        break;
      default : cardNum5 = "5";
        break;
    } //end of switch statement 5
    
    //determine if user has drawn a pair
    if (cardNum1 == cardNum2 || cardNum1 == cardNum2 || cardNum1 == cardNum3 || cardNum1 == cardNum4 || cardNum1 == cardNum5
    || cardNum2 == cardNum3 || cardNum2 == cardNum4 || cardNum2 == cardNum5 || cardNum3 == cardNum4 || cardNum3 == cardNum5
    || cardNum4 == cardNum5){
      pair = true;
      threeKind = false;
    }
    
    //determine if user has drawn three of a kind
    if ((cardNum1 == cardNum2 && cardNum1 == cardNum3) || (cardNum1 == cardNum2 && cardNum1 == cardNum4) || (cardNum1 == cardNum2
    && cardNum1 == cardNum5) || (cardNum1 == cardNum3 && cardNum1 == cardNum4) || (cardNum1 == cardNum3 && cardNum1 == cardNum5)
    || (cardNum1 == cardNum4 && cardNum1 == cardNum5) || (cardNum2 == cardNum3 && cardNum2 == cardNum4) || (cardNum2 == cardNum4
    && cardNum2 == cardNum5) || (cardNum3 == cardNum4 && cardNum3 == cardNum5)){
      threeKind = true;
      pair = false;
    }
    
    //determine if user has drawn a two pair
    if ((cardNum1 == cardNum2 && cardNum3 == cardNum4) || (cardNum1 == cardNum2 && cardNum3 == cardNum5) || (cardNum1 == cardNum2
    && cardNum4 == cardNum5) || (cardNum1 == cardNum3 && cardNum2 == cardNum4) || (cardNum1 == cardNum3 && cardNum2 == cardNum5)
    || (cardNum1 == cardNum3 && cardNum4 == cardNum5) || (cardNum1 == cardNum4 && cardNum2 == cardNum3) || (cardNum1 == cardNum4
    && cardNum2 == cardNum5) || (cardNum1 == cardNum4 && cardNum3 == cardNum5) || (cardNum1 == cardNum5 && cardNum2 == cardNum3)
    || (cardNum1 == cardNum5 && cardNum2 == cardNum4) || (cardNum1 == cardNum5 && cardNum3 == cardNum4) || (cardNum2 == cardNum3
    && cardNum4 == cardNum5) || (cardNum2 == cardNum4 && cardNum3 == cardNum5) || (cardNum2 == cardNum5 && cardNum3 == cardNum4)){
      twoPair = true;
      pair = false;
    }
    
    //set hand of the user
    if (threeKind == true && twoPair == true){
      hand = "three of a kind and two pair!";
    }
    else if (threeKind == true){
      hand = "three of a kind!";
    }
    else if (pair == true){
      hand = "a pair!";
    }
    else if (twoPair == true){
      hand = "two pair!";
    }
    else{
      hand = "a high card hand!";
    }
    
    //print results
    System.out.println("Your Random Cards Were:");
    System.out.println("The " + cardNum1 + " of " + suit1); //print card 1
    System.out.println("The " + cardNum2 + " of " + suit2); //print card 2
    System.out.println("The " + cardNum3 + " of " + suit3); //print card 3
    System.out.println("The " + cardNum4 + " of " + suit4); //print card 4
    System.out.println("The " + cardNum5 + " of " + suit5); //print card 5
    System.out.println("You have " + hand); //print hand
  } //end of main method
} //end of class