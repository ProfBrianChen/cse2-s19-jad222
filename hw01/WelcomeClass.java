// CSE 02 Homework 01
//Julia Damron
//1/29/19

public class WelcomeClass{
  
  public static void main(String[] args){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--A--D--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I am a member of the rowing team and play flute in the orchestra here at Lehigh!");
  }
}