// CSE2 HW2
// Julia Damron
// 2/5/19

public class Arithmetic{
	public static void main(String[] args){
		//Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

    //total cost of pants
    double costPants = pantsPrice * (double) numPants;
    //truncate costPants to 2 decimal places
    costPants = costPants * 100;
    costPants = (int) costPants;
    costPants = (double) costPants;
    costPants = costPants / 100;

    //total cost sweatshirts
    double costShirts = shirtPrice * (double) numShirts;
    //truncate costShirts to 2 decimal places
    costShirts = costShirts * 100;
    costShirts = (int) costShirts;
    costShirts = (double) costShirts;
    costShirts = costShirts / 100;

    //total cost belts
    double costBelts = beltCost * (double) numBelts;
    //truncate costShirts to 2 decimal places
    costBelts = costBelts * 100;
    costBelts = (int) costBelts;
    costBelts = (double) costBelts;
    costBelts = costBelts / 100;

    //sales tax = 6%
    double tax = 0.06;
    //pants tax
    double taxPants = tax * costPants;
    taxPants = taxPants * 100;
    taxPants = (int) taxPants;
    taxPants = (double) taxPants;
    taxPants = taxPants / 100;
    //sweatshirts tax
    double taxShirts = tax * costShirts;
    taxShirts = taxShirts * 100;
    taxShirts = (int) taxShirts;
    taxShirts = (double) taxShirts;
    taxShirts = taxShirts / 100;
    //belts tax
    double taxBelts = tax * costBelts;
    taxBelts = taxBelts * 100;
    taxBelts = (int) taxBelts;
    taxBelts = (double) taxBelts;
    taxBelts = taxBelts / 100;

    //total cost of purchase without tax
    double cost = costPants + costShirts + costBelts;

    //total sales tax
    double totalTax = taxPants + taxShirts + costBelts;

    //total cost including tax
    double totalCost = cost + totalTax;

    //print!
    System.out.println("The cost of buying 3 pairs of pants is $" + costPants + " and the sales tax is $" + taxPants);

    System.out.println("The cost of buying 2 sweatshirts is $" + costShirts + " and the sales tax is $" + taxShirts);

    System.out.println("The cost of buying 1 belt is $" + costBelts + " and the sales tax is $" + taxBelts);

    System.out.println("The total cost of the items is $" + cost);
    System.out.println("The total sales tax is $" + totalTax);
    System.out.println("The total cost of this purchase is $" + totalCost);

  }
}
