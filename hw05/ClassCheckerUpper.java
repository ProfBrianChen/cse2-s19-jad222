//CSE2 HW05
//Julia Damron
// 3/2/19

//CN, department, # of meetings per week, time class starts, instructor name, num students

import java.util.Scanner;

public class ClassCheckerUpper{
  public static void main(String[] args){
    Scanner scanny = new Scanner( System.in );
    double trashD;
    int cn, meets, time, studs;
    String dep, prof, trashS;
    
    //course number:
    System.out.print("Enter the course number: "); //prompts user to enter int
    //check if user input is an int:
    while (scanny.hasNextInt() == false){
      trashS = scanny.next(); //stores junk input
      System.out.println("Error: The input was not of integer type."); //error message
      System.out.print("Enter the course number: "); //prompts the user to enter int
    } //end of while loop
    cn = scanny.nextInt();
    
    //department:
    System.out.print("Enter the department name: "); //prompts user to enter string
    //check if user input is a string:
    while (scanny.hasNext() == false){
      trashD = scanny.nextDouble(); //stores junk input
      System.out.println("Error: The input was not of String type."); //error message
      System.out.print("Enter the department name: "); //prompts the user to enter string
    } //end of while loop
    dep = scanny.next();
    
    //num meetings:
    System.out.print("Enter the number of times the class meets per week: "); //prompts user to enter int
    //check if user input is an int:
    while (scanny.hasNextInt() == false){
      trashS = scanny.next(); //stores junk input
      System.out.println("Error: The input was not of integer type."); //error message
      System.out.print("Enter the number of times the class meets per week: "); //prompts the user to enter int
    } //end of while loop
    meets = scanny.nextInt();
    
    //class start time:
    System.out.print("Enter the start time with four digits (12:30 would be 1230): "); //prompts user to enter int
    //check if user input is an int:
    while (scanny.hasNextInt() == false){
      trashS = scanny.next(); //stores junk input
      System.out.println("Error: The input was not of integer type."); //error message
      System.out.print("Enter the start time with four digits (12:30 would be 1230): "); //prompts the user to enter int
    } //end of while loop
    time = scanny.nextInt();
    
    //instructor name:
    System.out.print("Enter the instructor name: "); //prompts user to enter string
    //check if user input is a string:
    while (scanny.hasNext() == false){
      trashD = scanny.nextDouble(); //stores junk input
      System.out.println("Error: The input was not of String type."); //error message
      System.out.print("Enter the instructor name: "); //prompts the user to enter string
    } //end of while loop
    prof = scanny.next();
    
    //num students:
    System.out.print("Enter the number of students enrolled: "); //prompts user to enter int
    //check if user input is an int:
    while (scanny.hasNextInt() == false){
      trashS = scanny.next(); //stores junk input
      System.out.println("Error: The input was not of integer type."); //error message
      System.out.print("Enter the number of students enrolled: "); //prompts the user to enter int
    } //end of while loop
    studs = scanny.nextInt();
    
    //print user input:
    System.out.println("CN: " + cn);
    System.out.println("Department: " + dep);
    System.out.println("Meetings per week: " + meets);
    System.out.println("Start Time: " + time);
    System.out.println("Instructor: " + prof);
    System.out.println("Students Enrolled: " + studs);
  } //end of main method
} //end of class