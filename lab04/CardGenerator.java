//CSE 2 Lab 4
//Julia Damron
// 2/15/19

public class CardGenerator{
  public static void main(String[] args){
    int num = (int) (Math.random() * 52) + 1; //generates random num between 1 and 52
    String suit; //introduces string used for suit
    //assign suits based on value of num
    if (num >= 1 && num <= 13){
      suit = "Diamonds";
    } //end of if statement
    else if (num > 13 && num <= 26){
      suit = "Clubs";
      num = num - 13;
    } //end of if statement
    else if (num > 26 && num < 38){
      suit = "Hearts";
      num = num - 26;
    } //end of if statement
    else{
      suit = "Spades";
      num = num - 38;
    } //end of else statement
    String cardNum = " "; //initializes string for number on cardNum
    //assign card numbers
    switch (num) {
      case 1 : cardNum = "Ace";
        break;
      case 2 : cardNum = "2";
        break;
      case 3 : cardNum = "3";
        break;
      case 4 : cardNum = "4";
        break;
      case 5 : cardNum = "5";
        break;
      case 6 : cardNum = "6";
        break;
      case 7 : cardNum = "7";
        break;
      case 8 : cardNum = "8";
        break;
      case 9 : cardNum = "9";
        break;
      case 10 : cardNum = "10";
        break;
      case 11 : cardNum = "Jack";
        break;
      case 12: cardNum = "Queen";
        break;
      case 13 : cardNum = "King";
        break;
    } //end of switch statement
    System.out.println("You picked the " + cardNum + " of " + suit); //print the card
  } //end of main method
} //end of class