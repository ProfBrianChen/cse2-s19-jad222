// CSE2 Lab 3
// Julia Damron
// 2/8/19

//allows program to access Scanner class
import java.util.Scanner;

public class Check{
  
  public static void main(String[] args){
  
    //create a new Scanner
    Scanner myScanner = new Scanner ( System.in );
    //prompt the user for check cost
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //use Scanner to store user input as checkCost
    double checkCost = myScanner.nextDouble();
    //prompt the user for the desired tip percentage
    System.out.print("Enter the percentage tip you wish to pay as a whole number (in the form xx): ");
    //use Scanner to store user input as tipPercent
    double tipPercent = myScanner.nextDouble();
    //convert tipPercent into decimal value
    tipPercent /= 100;
    //prompt the user for the number of people at dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    //use Scanner to store user input as numPeople
    int numPeople = myScanner.nextInt();

    //total cost of the dinner
    double totalCost;
    //cost each person must pay
    double costPerPerson;
    //to store values after the decimal point
    int dollars, dimes, pennies;

    //calculations
    //calculate total cost
    totalCost = checkCost * (1 + tipPercent);
    //calculate cost each person must pay
    costPerPerson = totalCost / numPeople;
    //store dollar amount of cost per person
    dollars = (int) costPerPerson;
    //store cents amount of cost per person
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    //print total cost per person
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
}