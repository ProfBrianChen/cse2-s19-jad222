//CSE2 Hw9
//Julia Damron
// 4/16/19

import java.util.Random;
import java.util.Scanner;

public class ArrayGames{
	public static void main( String[] args ){
		Scanner scanny = new Scanner( System.in ); //new scanner
		Random randy = new Random(); //new random
		boolean check = false; //for checking input type
		String answer = "";
		int choice = 0;
		int rando = 0;
		do{ //checks input for correct type
			System.out.print("Insert or shorten? "); //prompts user for insert or shorten
			if( !scanny.hasNext() ){ //user does not enter string
				System.out.println( "Error, try again." );
				continue;
			} //end of if
			answer = scanny.next();
			if( answer.equals( "insert" ) || answer.equals( "Insert" ) ){ //user chooses insert method
				choice = 1;
				check = true;
			} //end of if
			else if( answer.equals( "shorten" ) || answer.equals( "Shorten" ) ){ //user chooses shorten method
				choice = 2;
				check = true;
			} //end of else if
			else{ //user enters invalid string
				System.out.println( "Error, try again." );
			} //end of else
		}
		while( !check ); //condition for do
		if( choice == 1 ){ //user chooses insert
			int [] input1 = generate();
			int [] input2 = generate();
			insert( input1, input2 );
		} //end of if
		else if (choice == 2 ){ //user chooses shorten 
			int [] input = generate();
			rando = randy.nextInt( input.length + 5 );
			shorten( input, rando );
		} //end of else if
	} //end of main method
	public static int [] generate(){ //generates array of random size with random ints
		Random randy = new Random();
		int size = randy.nextInt( 11 ) + 10;
		int rando = 0;
		int [] array = new int [ size ];
		for( int i = 0; i < array.length; i++ ){ //fills array with random ints
			rando = randy.nextInt( size + 1 );
			array [i] = rando;
		} //end of for
    return array;
	} //end of generate method
	public static void print( int [] array ){ //prints input array
		System.out.print( "{" );
		for( int i = 0; i < array.length; i++ ){
			if( i == array.length - 1 ){
				System.out.print( array [i] );
			} //end of if
			else{
				System.out.print( array [i] + "," );
			} //end of else
		} //end of for
		System.out.println( "}" );
	} //end of print method
	public static void insert( int [] input1, int [] input2 ){ //insert method 
		Random randy = new Random();
		int spot = randy.nextInt( input1.length ); //selects random index in input1 to insert input2
		System.out.print( "Input 1: " );
		print( input1 ); //prints input1
		System.out.print( "Input 2: " );
		print( input2 ); //prints input2
		System.out.print( "Output: {" );
		for( int i = 0; i < spot + 1; i++ ){ //prints first part of input1
			System.out.print( input1 [i] + "," );
		} //end of for
		for( int i = 0; i < input2.length; i++ ){ //prints all of input2
			System.out.print( input2 [i] + "," );
		} //end of for
		for( int i = spot + 1; i < input1.length; i++ ){ //prints second part of input1
			if( i == ( input1.length - 1 ) ){
				System.out.print( input1 [i] );
			} //end of if
			else{
				System.out.print( input1 [i] + "," );
			} //end of else
		} //end of for
		System.out.println( "}" );
	} //end of insert method
	public static void shorten( int [] array, int index ){ //shorten method
		int [] array2 = new int [ array.length - 1 ]; //initializes new array that will be shortened
		System.out.print( "Input 1: " );
		print( array ); //prints input array
		System.out.println( "Input 2: " + index );
		if( index <= array.length - 1 ){ //shortens array if index is contained within size of array
			for( int i = 0; i < index; i++ ){ //prints first part of array up to index
				array2 [i] = array [i];
			} //end of for
			for( int i = index; i < array2.length; i++ ){ //prints second part of shortened array without array[index]
				array2 [i] = array [i + 1];
			} //end of for
			System.out.print( "Output: " );
			print( array2 );
		} //end if
		else{ //if index is not contained within size of array
			System.out.print( "Output: " );
			print( array );
		} //end of else
	} //end of shorten method
} //end of class