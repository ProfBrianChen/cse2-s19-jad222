//CSE2 HW8
//Julia Damron
// 4/9/19

import java.util.Random;

public class Letters{
	public static void main(String[] args){
		Random randy = new Random();
		int rando;
		String [] letters = { "A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z" };
		String [] array = new String [15];
		for (int i = 0; i < array.length; i++){ //assigns random values from letters to array
			rando = randy.nextInt(52);
			array [i] = letters [rando];
		}//end of for
		System.out.print("Random character array: "); //prints original array 
    for (int i = 0; i < 15; i++){
      System.out.print( array [i] );
    } //end for
    System.out.println();
		getAtoM( array );
		getNtoZ( array );
	} //end of main method
	
	public static void getAtoM( String [] array ){ //searches input array for characters a-m and returns answer
		String list = "";
    for (int i = 0; i < array.length; i++){
      list = list + array [i];
    }
    String atom = "";
    String [] letters = { "A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m" };
		for (int i = 0; i < letters.length; i++){
			if ( list.contains( letters[i]) ){
				atom = atom + letters[i];
			} //end of if
		} //end of for
		System.out.println("AtoM charaters: " + atom );
	} //end of a to m
	
	public static void getNtoZ( String [] array ){ //searches input array for characters n-z and returns answer
		String list = "";
    for (int i = 0; i < array.length; i++){
      list = list + array [i];
    }
 		String ntoz = "";
    String [] letters = { "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z" };
		for (int i = 0; i < letters.length; i++){
			if ( list.contains( letters[i]) ){
				ntoz = ntoz + letters[i];
			} //end of if
		} //end of for
		System.out.println("NtoZ characters: " + ntoz );
	} //end of n to z
} //end of class