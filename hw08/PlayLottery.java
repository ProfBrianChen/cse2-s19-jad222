// CSE2 HW8
// Julia Damron
// 4/9/19

import java.util.Scanner;
import javaa.util.Random;

public class PlayLottery{
  public static void main(String[] args){
    Scanner scanny = new Scanner( System.in );
    int [] user = new int [5];
    int num = 0;
    boolean wins = null;
    String check = "";
    System.out.print("Enter 5 different numbers between 0 and 59: ");
    num = scanny.nextInt();
    user [0] = num;
    check = (String) num + ".";
    for (int i = 1; i < 5; i++){
      System.out.print(", ");
      num = scanny.nextInt();
      user [i] = num;
      check = check + (String) num + ",";
    } //end for
    int [] winning = numbersPicked();
    wins = userWins();
    if (wins){
      System.out.println("You win!");
    } //end if
    else{
      System.out.println("You lose");
    } //end else
  } //end main method
  public static boolean userWins(int [] user, int [] winning){
    String check1 = "";
    String check2 = "";
    boolean win = null;
    for(int i = 0; i < 5; i++){
      if(user[i] != winning[i]){
        win = false;
        break;
      } //end if
    } //end for
    return win;
  } //end user wins
  public static int [] numbersPicked(){
    Random randy = new Random();
    String check = "";
    int lottery [] = new int [4];
    for(int i = 0; i < 5; i++){
      lottery [i] = randy.nextInt(60);
      /*while(check.contains(lottery[i])){
        lottery[i] = randy.nextInt(60);
      } //end while */
      check = check + (String) lottery[i] + ".";
    } //end for
    System.out.print("The winning numbers are: " + lottery[0] + ", " + lottery[1] + ", " + lottery [2] + ", " + lottery [3] + ", " + lottery[4]);
    return lottery;
  } //end numbers picked
} //end class