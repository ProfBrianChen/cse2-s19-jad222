//CSE2 Lab7
//Julia Damron
// 3/26/19

import java.util.Scanner; //imports scanner class

import java.lang.Math.*;

public class Area{
	public static double square(double side){
		double area = side * side;
		return area;
	} //end of square method
	public static double triangle(double side1, double side2, double side3){
		double p = ( side1 + side2 + side3 ) / 2;
		double area = p * ( p - side1 ) * ( p - side2 ) * ( p - side3 );
		area = Math.sqrt( area );
		return area;
	} //end of triangle method 
	public  static double circle(double radius){
		double area = Math.PI * Math.pow( radius, 2 );
		return area;
	} //end of circle method
	public static void main(String[] args){
		Scanner scanny = new Scanner( System.in);
		boolean check = false;
		String shape = "trash";
		double area = 0;
		double a = 0;
		double b = 0;
		double c = 0; 
		do{ //prompt user for shape
			System.out.print("Input your desired shape (square, triangle, or circle): ");
			shape = scanny.next(); //use to check for valid shape input
			if ( shape.equals( "square" ) ){
				check = true; //ends while
        continue;
			}
			else if ( shape.equals( "triangle" ) ){
				check = true; //ends while
        continue;
			}
			else if ( shape.equals( "circle" ) ){
				check = true; //ends while
        continue;
			}
      else{
        System.out.println("Error: not an acceptable shape. ");
      }
		} //end of do
		while(!check); //end of do while (while input is not correct shape)
		check = false;
		if ( shape.equals( "square" ) ){ //for square
			System.out.print("Input side length: ");
  		a = scanny.nextDouble();
			area = square(a);
		}
		else if ( shape.equals( "triangle" ) ){ //for triangle
				System.out.print("Input side length 1: ");
        a = scanny.nextDouble();
				System.out.print("Input side length 2: ");
        b = scanny.nextDouble();
				System.out.print("Input side length 3: ");
        c = scanny.nextDouble();
			area = triangle(a, b, c);
    }
		else if ( shape.equals( "circle" ) ){ //for circle
				System.out.print("Input radius: ");
        a = scanny.nextDouble();
			area = circle(a);
		}
		System.out.println("Area: " + area); //print the area
	} //end of main method
} //end of class